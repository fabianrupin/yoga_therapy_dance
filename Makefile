# Coding Style

.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
##
## Project setup
##---------------------------------------------------------------------------
cs: ## check cs problem
	./vendor/bin/php-cs-fixer fix --dry-run --stop-on-violation --diff

cs-fix: ## fix problems
	./vendor/bin/php-cs-fixer fix

cs-ci:
	./vendor/bin/php-cs-fixer fix src/ --dry-run --using-cache=no --verbose

fix: ## fix all
	./vendor/bin/php-cs-fixer fix
	./vendor/bin/phpstan

##
## database creation
##---------------------------------------------------------------------------
db-reset: ## Drop and recreate database
	./bin/console doctrine:database:drop --force --if-exists
	./bin/console doctrine:database:create

db-migrate: ## make migrations and add fixtures
	./bin/console doctrine:migrations:migrate -n

db-recreate: ## Reset and recreate database with migrations and fixtures
db-dev-create: db-reset db-migrate
	./bin/console doctrine:fixtures:load -n
##
## start and stop server
##---------------------------------------------------------------------------
start: ## start symfony server and gulp dev
	symfony run -d yarn encore dev --watch
	symfony serve --no-tls -d
	symfony open:local

stop: ## stop server
	symfony server:stop

##
## testing
##---------------------------------------------------------------------------
db-test-migrate: ## Create test database, loading test data and launch test
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
fixtures: ## Loading test data
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
	./bin/console doctrine:fixtures:load -n --env=test
test: ## Launching PHPUnit tests
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
	./bin/console doctrine:fixtures:load -n --env=test
	./bin/phpunit

deploy:
	git pull
	composer install --no-dev --optimize-autoloader
	yarn install
	yarn build
	symfony console c:c