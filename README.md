# [Yoga et Danse Thérapie](https://github.com/jackP8000/yoga_therapy_dance)

[Yoga et Danse Thérapie](https://github.com/jackP8000/yoga_therapy_dance) is a showcase site developed for a yoga and dance therapy activity.
This site was developed for  [Florine Rupin](https://yoga-et-danse-therapie.fr).


## Components and Versions
This project was created on April 11, 2020, and today integrates the following components: 
* Symfony 5.2, installed with website-skeleton dependencies

## Getting Started

To begin using this project, 
* Clone the repo: `git clone git@github.com:fabianrupin195/tikare.git`
* install back dependencies : `composer install`
* install front dependencies and build front files : 
  * `yarn install`
  * `yarn build`
* clear the cache : `./bin/console c:c --env=prod`

## Bugs and Issues

Have a bug or an issue with this project? 
[Open a new issue](https://github.com/jackP8000/yoga_therapy_dance/issues/new) here on github, or send a mail at
[fabian.rupin@wheelwork.fr](mailto:fabian.rupin@wheelwork.fr)

## Creator

Tikare was created by and is maintained by 
**[Fabian Rupin](https://www.malt.fr/profile/fabianrupin)**, Owner of [WheelWork](https://wheelwork.io/).

## Copyright and License
