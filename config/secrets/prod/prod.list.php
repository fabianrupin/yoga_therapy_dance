<?php

return [
    'ADMIN_EMAIL' => null,
    'APP_SECRET' => null,
    'MAILER_DSN' => null,
    'SENTRY_DSN' => null,
];
