<?php

namespace App\Controller;

use App\DTO\ContactFormDTO;
use App\Form\ContactFormType;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function index(Request $request, Mailer $mailer): Response
    {
        $contactFormDTO = new ContactFormDTO();

        $contactForm = $this->createForm(ContactFormType::class, $contactFormDTO);

        $contactForm->handleRequest($request);

        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $mailer->sendContactMessage($contactFormDTO);

            $this->addFlash('success', 'contact-form.success');

            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/index.html.twig', [
            'contactForm' => $contactForm->createView(),
        ]);
    }
}
