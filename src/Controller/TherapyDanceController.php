<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TherapyDanceController extends AbstractController
{
    #[Route('/danse-thérapie', name: 'therapy_dance')]
    public function index(): Response
    {
        return $this->render('therapy_dance/index.html.twig');
    }
}
