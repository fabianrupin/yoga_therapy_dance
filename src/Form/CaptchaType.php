<?php

namespace App\Form;

use App\Contracts\CaptchaInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Traversable;

class CaptchaType extends AbstractType
{
    private const SESSION_KEY = 'captcha_answer';

    protected CaptchaInterface $captcha;

    protected string $question;

    protected string $previousAnswer = '';

    protected string $nextAnswer;

    /**
     * @param RequestStack                  $requestStack
     * @param Traversable<CaptchaInterface> $captchas
     */
    public function __construct(private RequestStack $requestStack, Traversable $captchas)
    {
        $captchas = iterator_to_array($captchas);

        $this->captcha = $captchas[array_rand($captchas)];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        list($this->question, $this->nextAnswer) = $this->captcha->getChallenge();
        $this->handleSession();
    }

    protected function handleSession(): void
    {
        if (is_string($previousAnswer = $this->requestStack->getSession()->get(self::SESSION_KEY))) {
            $this->previousAnswer = $previousAnswer;
        }

        $this->requestStack->getSession()->set(self::SESSION_KEY, $this->nextAnswer);
    }

    public function validateCaptcha(?string $data, ExecutionContextInterface $context): void
    {
        if (is_null($data)) {
            $context
                ->buildViolation('captcha_null')
                ->setTranslationDomain('captcha')
                ->addViolation();

            return;
        }

        if (false === $this->captcha->checkAnswer($data, $this->previousAnswer)) {
            $context
                ->buildViolation('captcha_invalid')
                ->setTranslationDomain('captcha')
                ->addViolation();
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['captcha_question'] = $this->question;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'autocapitalize' => 'none',
                'autocorrect' => 'off',
                'spellcheck' => 'false',
            ],
            'constraints' => [
                new Callback([
                    'callback' => [$this, 'validateCaptcha'],
                ]),
            ],
            'mapped' => false,
            'required' => false,
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
