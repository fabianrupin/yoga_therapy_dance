<?php

namespace App\Service\Captcha;

use App\Contracts\CaptchaInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class Captcha implements CaptchaInterface
{
    public function __construct(protected DictionaryService $dictionary, protected TranslatorInterface $translator)
    {
    }

    protected function getAnswer(string $word, int $letterIndex): string
    {
        if (0 > $letterIndex) {
            $letterIndex = abs($letterIndex) - 1;
            $word = strrev($word);
        }

        $answer = '';

        for ($i = $letterIndex; $i >= 0; --$i) {
            $answer = $word[strcspn($word, get_class($this)::LETTERS)];
            $word = preg_replace('/'.$answer.'/', '_', $word, 1) ?? '';
        }

        return $answer;
    }

    public function checkAnswer(string $givenAnswer, string $expectedAnswer): bool
    {
        return strtoupper($givenAnswer) === strtoupper($expectedAnswer);
    }

    /**
     * @return array<string>
     */
    protected function getAbstractChallenge(): array
    {
        $letterIndex = (int) array_rand(get_class($this)::INDEX_MAPPING);
        $word = $this->dictionary->getRandomWord();

        return [
            $this->getQuestion($word, $letterIndex),
            $this->getAnswer($word, $letterIndex),
        ];
    }

    protected function getQuestion(string $word, int $letterIndex): string
    {
        return $this->translator->trans('captcha_sentence', [
            'index' => $this->translator->trans(
                sprintf('captcha_%s', get_class($this)::INDEX_MAPPING[$letterIndex]),
                [],
                'captcha'
            ),
            'letter' => $this->translator->trans(get_class($this)::CAPTCHA_TEXT, [], 'captcha'),
            'word' => $word,
        ], 'captcha');
    }
}
