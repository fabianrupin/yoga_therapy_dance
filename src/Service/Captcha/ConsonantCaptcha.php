<?php

namespace App\Service\Captcha;

use App\Contracts\CaptchaInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsonantCaptcha extends Captcha implements CaptchaInterface
{
    protected const LETTERS = 'BCDFGHJKLMNPQRSTVWXZ';
    protected const CAPTCHA_TEXT = 'captcha_consonant';

    protected const INDEX_MAPPING = [
        '0' => 'first',
        '1' => 'second',
        '2' => 'third',
        '-1' => 'last',
    ];

    public function __construct(protected DictionaryService $dictionary, protected TranslatorInterface $translator)
    {
        parent::__construct($this->dictionary, $this->translator);
    }

    /**
     * @return array<string>
     */
    public function getChallenge(): array
    {
        return $this->getAbstractChallenge();
    }
}
