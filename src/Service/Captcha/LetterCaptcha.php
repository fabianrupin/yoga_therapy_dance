<?php

namespace App\Service\Captcha;

use App\Contracts\CaptchaInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class LetterCaptcha extends Captcha implements CaptchaInterface
{
    protected const CAPTCHA_TEXT = 'captcha_letter';
    protected const INDEX_MAPPING = [
        '0' => 'first',
        '1' => 'second',
        '2' => 'third',
        '3' => 'fourth',
        '4' => 'fifth',
        '-1' => 'last',
    ];

    public function __construct(protected DictionaryService $dictionary, protected TranslatorInterface $translator)
    {
        parent::__construct($this->dictionary, $this->translator);
    }

    /**
     * @return array<string>
     */
    public function getChallenge(): array
    {
        $letterIndex = array_rand(self::INDEX_MAPPING);
        $word = $this->dictionary->getRandomWord();

        return [
            $this->getQuestion($word, $letterIndex),
            $this->getAnswer($word, $letterIndex),
        ];
    }

    protected function getAnswer(string $word, int $letterIndex): string
    {
        if (0 > $letterIndex) {
            $letterIndex = abs($letterIndex) - 1;
            $word = strrev($word);
        }

        return $word[$letterIndex];
    }
}
