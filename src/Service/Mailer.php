<?php

namespace App\Service;

use App\DTO\ContactFormDTO;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class Mailer
{
    public function __construct(
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private string $emailAdmin,
        private string $emailNameAdmin,
        private LoggerInterface $logger
    ) {
    }

    public function sendContactMessage(contactFormDTO $contactFormDTO): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject($this->translator->trans('mailer.contact-message.subject', [], 'mailer'))
            ->htmlTemplate('email/contact.html.twig')
            ->context(['contactFormDTO' => $contactFormDTO, 'emailTo' => $this->emailAdmin]);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            $this->logger->critical('Sending email seems to be impossible: '.$exception);
        }

        return $email;
    }
}
